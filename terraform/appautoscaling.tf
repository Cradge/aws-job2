//AppAutoscaling Policies and Cloudwatch Alarms
resource "aws_appautoscaling_target" "ecs_service_target" {
  min_capacity       = 1
  max_capacity       = 6
  resource_id        = "service/${var.cluster_name}/${var.service_name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
  role_arn           = aws_iam_role.ecsAppAutoscaleRole.arn

  depends_on = [aws_ecs_service.petclinic_service]
}

resource "aws_appautoscaling_policy" "appautoscaling_up" {
  name               = "appautoscaling_policy_up"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.ecs_service_target.id
  scalable_dimension = aws_appautoscaling_target.ecs_service_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_service_target.service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 120
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }

  depends_on = [aws_appautoscaling_target.ecs_service_target]
}

resource "aws_cloudwatch_metric_alarm" "alb_requests_up" {
  alarm_name          = "alb_requests_up"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "RequestCount"
  namespace           = "AWS/ApplicationELB"
  period              = "60"
  statistic           = "Sum"
  threshold           = "100"

  dimensions = {
    LoadBalancer = aws_alb.application_load_balancer.arn_suffix
    TargetGroup  = aws_lb_target_group.target_group.arn_suffix
  }

  alarm_description = "This metric monitors requests to ALB"
  alarm_actions     = [aws_appautoscaling_policy.appautoscaling_up.arn]
}

resource "aws_appautoscaling_policy" "appautoscaling_down" {
  name               = "appautoscaling_policy_down"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.ecs_service_target.id
  scalable_dimension = aws_appautoscaling_target.ecs_service_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_service_target.service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 120
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }

  depends_on = [aws_appautoscaling_target.ecs_service_target]
}

resource "aws_cloudwatch_metric_alarm" "alb_requests_down" {
  alarm_name          = "alb_requests_down"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "RequestCount"
  namespace           = "AWS/ApplicationELB"
  period              = "60"
  statistic           = "Sum"
  threshold           = "10"

  dimensions = {
    LoadBalancer = aws_alb.application_load_balancer.arn_suffix
    TargetGroup  = aws_lb_target_group.target_group.arn_suffix
  }

  alarm_description = "This metric monitors requests to ALB"
  alarm_actions     = [aws_appautoscaling_policy.appautoscaling_down.arn]
}

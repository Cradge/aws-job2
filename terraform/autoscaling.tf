//Launch Configuration
data "aws_ami" "ecs_optimized_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["amazon"]
}

resource "aws_launch_configuration" "ecs_launch_config" {
  image_id                    = data.aws_ami.ecs_optimized_ami.id
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  lifecycle {
    create_before_destroy = true
  }
  user_data = <<EOF
#!/bin/bash
echo ECS_CLUSTER=${var.cluster_name} >> /etc/ecs/ecs.config
EOF

  security_groups      = [aws_security_group.ec2_security_group.id]
  iam_instance_profile = aws_iam_instance_profile.ec2_ecs_agent.arn
}

//Autoscaling Group
resource "aws_autoscaling_group" "autoscaling_group" {
  name                 = "autoscaling_group"
  vpc_zone_identifier  = aws_subnet.subnet_public.*.id
  launch_configuration = aws_launch_configuration.ecs_launch_config.name

  desired_capacity     = 2
  min_size             = 1
  max_size             = 6

  termination_policies = [
    "OldestInstance"
  ]

  lifecycle {
    create_before_destroy = true
  }

  health_check_type = "ELB"

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]

  metrics_granularity = "1Minute"

  tag {
    key   = "Name"
    value = var.cluster_name

    propagate_at_launch = true
  }
}

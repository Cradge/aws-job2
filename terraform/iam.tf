// IAM ECS-Tasks
resource "aws_iam_role" "ecsTaskExecutionRole" {
  name               = "ecsExecutionTaskRole"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy_ecs.json
}

data "aws_iam_policy_document" "assume_role_policy_ecs" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole_policy" {
  role       = aws_iam_role.ecsTaskExecutionRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

//IAM EC2
resource "aws_iam_role" "ecsEC2Role" {
  name               = "ecsRoleForEC2"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy_ec2.json
}

data "aws_iam_policy_document" "assume_role_policy_ec2" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "ecsEC2Role_policy" {
  role       = aws_iam_role.ecsEC2Role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "ec2_ecs_agent" {
  name = "ec2-ecs-agent"
  role = aws_iam_role.ecsEC2Role.name
}

//IAM AppAutoscaling
resource "aws_iam_role" "ecsAppAutoscaleRole" {
  name               = "ecsServiceAppAutoscale"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy_autoscale.json
}

data "aws_iam_policy_document" "assume_role_policy_autoscale" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["application-autoscaling.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "ecsAppAutoscaleRole_policy" {
  role       = aws_iam_role.ecsAppAutoscaleRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceAutoscaleRole"
}

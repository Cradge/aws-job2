//Provider
provider "aws" {
  region = "us-east-2"
}

//Backend
terraform {
  backend "s3" {
    bucket = "mike-r-terraform-state"
    key    = "terraform/terraform.tfstate"
    region = "us-east-2"
  }
}

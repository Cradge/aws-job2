//Database Instance
resource "aws_db_subnet_group" "db_subnet_group" {
  subnet_ids = aws_subnet.subnet_private.*.id
}

resource "aws_db_instance" "mysql" {
  identifier              = "mysql"
  allocated_storage       = 5
  multi_az                = true
  engine                  = "mysql"
  engine_version          = "5.7"
  instance_class          = "db.t2.micro"
  db_name                 = "petclinic"
  username                = "petclinic"
  password                = "petclinic"
  port                    = 3306
  db_subnet_group_name    = aws_db_subnet_group.db_subnet_group.id
  vpc_security_group_ids  = [aws_security_group.rds_security_group.id]
  skip_final_snapshot     = true

  backup_retention_period = 0 //to save time (for me)
}

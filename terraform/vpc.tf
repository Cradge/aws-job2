//VPC
resource "aws_vpc" "vpc_main" {
  cidr_block           = "10.1.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
}

//Internet Gateway
resource "aws_internet_gateway" "net_gateway_main" {
  vpc_id = aws_vpc.vpc_main.id
}

//Subnets
resource "aws_subnet" "subnet_private" {
  vpc_id            = aws_vpc.vpc_main.id
  count             = length(var.private_subnets)
  cidr_block        = element(var.private_subnets, count.index)
  availability_zone = element(var.availability_zones, count.index)
}

resource "aws_subnet" "subnet_public" {
  vpc_id            = aws_vpc.vpc_main.id
  count             = length(var.public_subnets)
  cidr_block        = element(var.public_subnets, count.index)
  availability_zone = element(var.availability_zones, count.index)

  map_public_ip_on_launch = true
}

//Route Tables
resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.vpc_main.id
}

resource "aws_route" "route" {
  route_table_id         = aws_route_table.route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.net_gateway_main.id
}

resource "aws_route_table_association" "route_table_association" {
  count          = length(var.public_subnets)
  subnet_id      = element(aws_subnet.subnet_public.*.id, count.index)
  route_table_id = aws_route_table.route_table.id
}

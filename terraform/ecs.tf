//ECS Cluster
resource "aws_ecs_cluster" "ecs_cluster" {
  name = var.cluster_name
}

//Log Group
resource "aws_cloudwatch_log_group" "log-group" {
  name = "ECS_Logs"
}

//Task Definition
resource "aws_ecs_task_definition" "task_definition" {
  family                   = "petclinic_task"
  requires_compatibilities = ["EC2"]
  network_mode             = "awsvpc"
  memory                   = 512
  cpu                      = 512
  execution_role_arn       = aws_iam_role.ecsTaskExecutionRole.arn
  task_role_arn            = aws_iam_role.ecsTaskExecutionRole.arn

  container_definitions = jsonencode([
    {
      name         = "petclinic"
      image        = var.image_path
      environment  = [{ "name" : "MYSQL_HOST", "value" :  "${aws_db_instance.mysql.endpoint}"}]
      memory       = 512
      cpu          = 512
      essential    = true
      portMappings = [
        {
          containerPort = 8080
          hostPort      = 8080
        }
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options   = {
          awslogs-group         = aws_cloudwatch_log_group.log-group.id
          awslogs-region        = "us-east-2"
          awslogs-stream-prefix = "ecs"
        }
      }
    }
    ])
}

//ECS Service
resource "aws_ecs_service" "petclinic_service" {
  name                 = var.service_name
  cluster              = aws_ecs_cluster.ecs_cluster.id
  task_definition      = aws_ecs_task_definition.task_definition.arn
  launch_type          = "EC2"
  scheduling_strategy  = "REPLICA"
  desired_count        = 2
  force_new_deployment = true

  network_configuration {
    subnets          = aws_subnet.subnet_private.*.id
    assign_public_ip = false
    security_groups  = [
      aws_security_group.ec2_security_group.id,
      aws_security_group.load_balancer_security_group.id
    ]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.target_group.arn
    container_name   = "petclinic"
    container_port   = 8080
  }

  depends_on = [aws_lb_listener.listener]
}

//ECS Capacity Provider
resource "aws_ecs_cluster_capacity_providers" "ecs_cluster_capacity_providers" {
  cluster_name = aws_ecs_cluster.ecs_cluster.name

  capacity_providers = [aws_ecs_capacity_provider.ecs_capacity_provider.name]

  default_capacity_provider_strategy {
    base = 1
    weight = 100
    capacity_provider = aws_ecs_capacity_provider.ecs_capacity_provider.name
  }
}

resource "aws_ecs_capacity_provider" "ecs_capacity_provider" {
  name = "capacity-provider"

  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.autoscaling_group.arn
    managed_termination_protection = "DISABLED"

    managed_scaling {
      status                 = "ENABLED"
      instance_warmup_period = 240
      target_capacity        = 70
    }
  }
}

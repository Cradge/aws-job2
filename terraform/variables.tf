variable "private_subnets" {
  description = "List of private subnets"
  default = [ "10.1.10.0/24", "10.1.20.0/24" ]
}

variable "public_subnets" {
  description = "List of public subnets"
  default = [ "10.1.30.0/24", "10.1.40.0/24" ]
}

variable "availability_zones" {
  description = "List of availability zones"
  default = [ "us-east-2a", "us-east-2b" ]
}

variable "cluster_name" {
  description = "Name of the cluster"
  default = "MyCluster"
}

variable "service_name" {
  description = "Name of the service"
  default = "petclinic_service"
}

variable "image_path" {
  description = "Path to the image"
  default = "registry.gitlab.com/cradge/aws-job1:main"
}
